<?php
/**
 * Zip Assets plugin for Craft CMS 3.x
 *
 * Zip and download assets on the fly
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 mike Stivala
 */

namespace stivala\zipassets\services;

use Craft;
use Exception;
use ZipArchive;
use craft\base\Component;
use craft\elements\Asset;
use stivala\zipassets\ZipAssets;

/**
 * @author    mike Stivala
 * @package   ZipAssets
 * @since     1.0.0
 */
class Zip extends Component
{
    public function zipAssets($assetIds, $filename)
    {
        $zip = $this->getZipArchive($filename);

        foreach (Asset::find()->id($assetIds)->all() as $asset) {
            $zip->addFromString($asset->filename, stream_get_contents($asset->getStream()));
        }
        
        $path = $zip->filename;
        $zip->close();

        return $path;
    }

    private function getZipArchive($filename)
    {
        $zip = new ZipArchive();

        $destinationFilename = Craft::$app->path->getTempPath().$filename.'_'.time().'.zip';

        if (! $zip->open($destinationFilename, $zip::CREATE) === true) {
            throw new Exception(Craft::t('Failed to generate the zipfile'));
        }

        return $zip;
    }
}
