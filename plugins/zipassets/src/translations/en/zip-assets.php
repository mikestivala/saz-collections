<?php
/**
 * Zip Assets plugin for Craft CMS 3.x
 *
 * Zip and download assets on the fly
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 mike Stivala
 */

/**
 * @author    mike Stivala
 * @package   ZipAssets
 * @since     1.0.0
 */
return [
    'Zip Assets plugin loaded' => 'Zip Assets plugin loaded',
];
