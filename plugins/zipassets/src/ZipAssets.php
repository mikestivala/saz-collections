<?php
/**
 * Zip Assets plugin for Craft CMS 3.x
 *
 * Zip and download assets on the fly
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 mike Stivala
 */

namespace stivala\zipassets;

use stivala\zipassets\services\Zip;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\UrlManager;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;

/**
 * Class ZipAssets
 *
 * @author    mike Stivala
 * @package   ZipAssets
 * @since     1.0.0
 *
 * @property  ZipAssetsService $zipAssetsService
 */
class ZipAssets extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var ZipAssets
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        $this->setComponents([
            'zip' => Zip::class,
        ]);

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['siteActionTrigger1'] = 'zip-assets/zip-assets-controller';
            }
        );

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['cpActionTrigger1'] = 'zip-assets/zip-assets-controller/do-something';
            }
        );

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'zip-assets',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
