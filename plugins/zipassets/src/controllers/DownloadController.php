<?php
/**
 * Zip Assets plugin for Craft CMS 3.x
 *
 * Zip and download assets on the fly
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 mike Stivala
 */

namespace stivala\zipassets\controllers;

use stivala\zipassets\ZipAssets;

use Craft;
use craft\web\Controller;

/**
 * @author    mike Stivala
 * @package   ZipAssets
 * @since     1.0.0
 */
class DownloadController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'download'];

    // Public Methods
    // =========================================================================

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        // Get wanted filename
        $filename = Craft::$app->request->getRequiredParam('filename');
        // Get file id's
        $assetIds = Craft::$app->request->getRequiredParam('files');
        // Generate zipfile
        $path = ZipAssets::getInstance()->zip->zipAssets($assetIds, $filename);
        // Download it
        Craft::$app->response->sendFile($path);
        // Delete it
        unlink($path);
    }
}
